<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\DatatablesController;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/login', [LoginController::class, 'index']);


Route::get('/', function () {
    return response(['message'=>'Unauthenticated'], 401);
})->name('login');

$router->group(['prefix' => 'user'], function () use ($router) {
    $router->post('/create', [UsersController::class, 'create']);
    $router->get('/detail', [UsersController::class, 'getUser'])->middleware(['auth:sanctum']);

});

$router->group(['prefix' => 'datatables','middleware'=>['auth:sanctum']], function () use ($router) {
    $router->get('/users', [DatatablesController::class, 'users']);
});
