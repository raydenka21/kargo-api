<?php

namespace App\Http\Controllers;
use App\Models\Users;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;



class UsersController extends Controller
{
    /**
     * @OA\Get(
     *     path="/greet2",
     *     tags={"greeting"},
     *     summary="Returns a Sample API response",
     *     description="A sample greeting to test out the API",
     *     operationId="greet",
     *     @OA\Parameter(
     *          name="firstname",
     *          description="nama depan",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="lastname",
     *          description="nama belakang",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    function __construct(){
    }
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username'=> 'required|regex:/^[a-zA-Z0-9_\-]*$/',
            'email'=> 'required|email',
            'first_name'=> 'required|regex:/^[a-zA-Z0-9_\-]*$/',
            'last_name'=> 'required|regex:/^[a-zA-Z0-9_\-]*$/',
            'password'=> 'required',
            'handphone' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            $response = $this->responseApp(status:NOT,message:$validator->messages());
            return response()->json($response);
        }
        $data = [
            'username'=> $request->input('username'),
            'email'=>$request->input('email'),
            'first_name'=>$request->input('first_name'),
            'last_name'=>$request->input('last_name'),
            'password'=> $this->encrypt(key:$request->input('password')),
            'handphone'=>$request->input('handphone'),
            'status' => 'active'
        ];
        $response = $this->responseApp(status:OK);
        try {
            DB::transaction(function () use ($data) {
                Users::Create($data);
            }, 2);

        } catch (QueryException $e) {
            $duplicateInfo =$this->duplicateSqlCode(message:$e->getMessage());
            $response = $this->responseApp(status:NOT);
            if($duplicateInfo){
                $response = $this->responseApp(status:NOT,message:'Username / Email exist');
            }
        }
        return response()->json($response);
    }
    public function getUser(Request $request){
        $getDetail = $this->userDetail($request);
        $response = $this->responseApp(status:OK,data:$getDetail);
        return response()->json($response);
    }

}
