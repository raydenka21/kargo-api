<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;



class Controller extends BaseController
{
    /**
     * @OA\Info(title="My First API", version="0.1")
     */
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    function __construct(){

    }

    public function responseApp($status=null,$message=null,$data=null){
        $response = [
            'status'=> $status,
            'message'=>$message,
            'data'=> $data
        ];
        return $response;
    }
    public function encrypt($key){
        $encrypt = sha1(md5(env('ENCRYPTYON').$key));
        return $encrypt;
    }
    public function duplicateSqlCode($message){
        if (strpos($message, 'SQLSTATE[23000]') !== false) {
           return true;
        }
        return false;
    }
    public function userDetail($request){
        $user = $request->user() ?? false;
        return $user;

    }
}
