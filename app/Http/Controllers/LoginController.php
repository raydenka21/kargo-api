<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use App\Models\Users;




class LoginController extends Controller
{
    /**
     * @OA\Get(
     *     path="/greet1",
     *     tags={"greeting"},
     *     summary="Returns a Sample API response",
     *     description="A sample greeting to test out the API",
     *     operationId="greet",
     *     @OA\Parameter(
     *          name="firstname",
     *          description="nama depan",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="lastname",
     *          description="nama belakang",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    function __construct(){

    }
    public function index(Request $request)
    {
        $credentials = $request->only('username', 'password');
        $validator = Validator::make($credentials, [
            'username' => 'required',
            'password' => 'required|string|min:1|max:50'
        ]);
        if ($validator->fails()) {
            $response = $this->responseApp(status:NOT,message:$validator->messages());
            return response()->json($response);
        }
        $password = $this->encrypt(key:$request->input('password'));
        $username = $request->input('username');
        $user = Users::where('username',$username)
            ->orWhere('email',$username)
            ->where('password',$password)
            ->first(['id','username','email','handphone']);
        $token = null;
        if(!$user){
            $response = $this->responseApp(status:NOT,message:'username / password is wrong');
        }else {
            $token = $user->createToken(env('SECRET_KEY'))->plainTextToken;
            $response = $this->responseApp(status: OK, message: 'Login Successfully', data: ['token' => $token]);
        }
        return response()->json($response);
    }
}
